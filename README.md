Установка:

1. Скачать и установить Python 3.6 с опцией добавления "bin" в переменную среды "path"
2. pip install pyyaml
2. git clone https://miatey@bitbucket.org/3lfar7/tripadvparser.git
3. cd tripadvparser
4. python tripadvparser.py clean

Использование:

python.exe tripadvparser.py команда, где команда:

* fetch_hotels - добавление отелей в бд   
* fetch_photos - закачка фото по отелям которые уже есть в бд
* fetch_prices - обновление цен по отелям которые уже есть в бд
* clean - удаление бд и фоток

conf.yaml конфигурационный файл

поле path это список локаций(страны, города) или конкретных отелей, он берется с трипадвизора

результат работы по умолчанию находится в директории output (можно поменять в конфигурации)

бд - output/tripadvisor.db

лог с ошибками - output/errors.log

клиент для просмотра бд http://sqlitebrowser.org/

Под виндой я парсер не проверял